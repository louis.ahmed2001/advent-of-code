with open('day7.txt') as file:
	data = file.readlines()
	data = [line.strip() for line in data]
	
def get_num_bags(colour):
	lines = [line for line in data if colour in line and line.index(colour) != 0]
	
	allColours = []
	
	if len(lines) == 0:
		return[]
	
	else:
		colours = [line[:line.index(' bags')] for line in lines]
		colours = [colour for colour in colours if colour not in allColours]
		
		for colour in colours:
			allColours.append(colour)
			bags = get_num_bags(colour)
			
			allColours += bags
		
		uniqueColours = []
		for colour in allColours:
			if colour not in uniqueColours:
				uniqueColours.append(colour)
		
		return uniqueColours
	
colours = get_num_bags('shiny gold')
print(len(colours))

def get_bag_count(colour):
	rule = ''
	for line in data:
		if line[:line.index(' bags')] == colour:
			rule = line
	
	if 'no' in rule:
		return 1
	
	rule = rule[rule.index('contain')+8:].split()
	
	total = 0
	i = 0
	while i < len(rule):
		count = int(rule[i])
		colour = rule[i+1] + ' ' + rule[i+2]
		
		total += count * get_bag_count(colour)
		
		i += 4
	return total + 1

count = get_bag_count('shiny gold')
print(count-1)
