def part1(input1):
	with open('day3two.txt') as file:
		count = 0
		data = file.readlines()
		grid = []
		for i in range(0, 2000):
			grid.append([])
			for j in range(0, 2000):
				grid[i].append(0)		
		
		for claim in data:
			nums = []
			split1 = claim.split(' ')
			split2 = split1[2].split(',')
			split3 = split2[1].split(':')
			split4 = split1[3].split('x')
			split5 = split4[1].split('\n')
			
			nums.append(split2[0])
			nums.append(split3[0])
			nums.append(split4[0])
			nums.append(split5[0])

			for i in range(0, int(nums[2])):
				for j in range(0, int(nums[3])):
					grid[int(nums[0])+i][int(nums[1])+j] += 1
	
		for line in grid:
			for element in line:
				if element > 1:
					count += 1

		print(count)
		
part1('')
