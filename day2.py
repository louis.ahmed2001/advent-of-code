total = 0
total2 = 0
with open("day2.txt") as file:
	for line in file:
		parsed = line.split(" ")
		chunk1 = parsed[0]
		low = int(chunk1.split("-")[0])
		high = int(chunk1.split("-")[1])
		chunk2 = parsed[1]
		letter = chunk2[0]
		password = parsed[2]
		finds = 0
		for character in password:
			if character == letter:
				finds += 1
		if finds >= low and finds <= high:
			total += 1
		one = password[low - 1]
		two = password[high - 1]
		if (letter == one) ^ (letter == two):
			total2 += 1
print(total)
print(total2)
