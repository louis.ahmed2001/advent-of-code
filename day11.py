with open('day11.txt') as file:
	data = file.readlines()
	data = [list(line.strip()) for line in data]
	original = data.copy()

def get_num_occupied():
	count = 0
	for row in data:
		for seat in row:
			if seat == '#':
				count += 1
	return count

def get_adjacent_count(row,col):
	count = 0
	currentRow = data[row]
	
	if col-1 >= 0:
		if currentRow[col-1] == '#': 
			count += 1
	
	if col+1 <= len(currentRow)-1:
		if currentRow[col+1] == '#':
			count += 1
	
	if row-1 >= 0:
		aboveRow = data[row-1]
		if aboveRow[col] == '#':
			count += 1
			
		if col-1 >= 0:
			if aboveRow[col-1] == '#':
				count += 1
		
		if col+1 <= len(aboveRow)-1:
			if aboveRow[col+1] == '#':
				count += 1
	
		if row+1 <= len(data)-1:
			belowRow = data[row+1]
			if belowRow[col] == '#':
				count += 1
			
			if col-1 >= 0:
				if belowRow[col-1] == '#':
					count += 1
			
			if col+1 <= len(belowRow)-1:
				if belowRow[col+1] == '#':
					count += 1
		
		return count

def run_rules():
	newSeating = []
	
	for row in range(len(data)):
		currentRow = data[row]
		newRow = []
		
		for col in range(len(currentRow)):
			if currentRow[col] == '.':
				newRow.append('.')
				continue
			
			adjacentCount = get_adjacent_count(row,col)
			
			if currentRow[col] == 'L' and adjacentCount == 0:
				newRow.append('#')
			
			elif currentRow[col] == '#' and adjacentCount >= 4:
				newRow.append('L')
			
			else:
				newRow.append(currentRow[col])
		newSeating.append(newRow)
	for i in range(len(data)):
		data[i] = newSeating[i]

def get_final_count():
	prev = data.copy()
	run_rules()
	
	while data != prev:
		prev = data.copy()
		run_rules()
	return get_num_occupied()

print(get_final_count())
